#!/usr/bin/env python
'''
Takes a dicom file as input and checks headers for modality.
Returns 1 if a secondary scan, 0 if not
'''
import os
import sys
from dicom import read_file

try:
    dcm_dir = sys.argv[1]
except IndexError as e:
    print "Need a dicom directory as input"
    sys.exit(-1)

dcm_file = os.listdir(dcm_dir)[0]
dcm = read_file(os.path.join(dcm_dir, dcm_file))

if dcm.Modality == 'SC' or dcm.Modality == 'SR':
    sys.exit(1)

sys.exit(0)
